from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import serializers
from .models import User, Student
from rest_framework import status
from .serializer import UserSerializer, StudentSerializer
from django.contrib.auth.hashers import make_password
from rest_framework.viewsets import ModelViewSet
from .permission import IsAdminUser, IsNormalUser


# class Users(APIView):
#     """
#     View to list all users in the system.
#
#     * Requires token authentication.
#     * Only admin users are able to access this view.
#     """
#
#     def get(self, request):
#         """
#         Return a list of all users.
#         """
#
#         # usernames = {'username':'nabinstha', 'email':"shresthanabin94@gmail.com", 'address':'kathmandu, thankot'}
#         # Userdata = []
#         user = User.objects.all()  #request.GET.get('username')
#
#         serializer = UserSerializer(user, many=True)
#
#         # for user in usernames:
#         #     Userdata.append({
#         #         'username': user.username,
#         #         'email': user.email,
#         #         'address': user.address,
#         #     })
#
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     def post(self, request):
#         # username = request.data.get('username')
#         # email = request.data.get('email')
#         # address = request.data.get('address')
#         password = request.data.pop('password')
#         serializer = UserSerializer(data=request.data)
#         if serializer.is_valid():
#             validated_data = serializer.validated_data
#             u = User.objects.create(**validated_data)
#             u.password = make_password(password=password, salt=None, hasher='default')
#             u.save()
#         data_comes = "successfully"
#         return Response({'meassage': data_comes})
# modelviewset


class UserView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class StudentView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    def get_permissions(self):
        get_list = []
        if self.action == 'list':
            get_list.append(IsAdminUser)
        else:
            IsNormalUser()
        return [permission() for permission in get_list]

    def list(self, request, *args, **kwargs):
        request_get = self.get_serializer(self.get_queryset(), many= True)
        r = {
            "message": "Success",
            "data": request_get.data
        }
        return Response(r)
