from django.db import models


# Create your models here.


class User(models.Model):
    username = models.CharField(max_length=10)
    email = models.EmailField()
    address = models.CharField(max_length=200)
    password = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.password


class Student(models.Model):
    student_name = models.CharField(max_length=20)
    student_address = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)









