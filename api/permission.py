from  rest_framework.permissions import BasePermission


class IsAdminUser(BasePermission):
    def has_permission(self, request, view):
        if request.user.groups.all()[0].name == 'Admin':
            return True
        else:
            return False


class IsNormalUser(BasePermission):
    def has_permission(self, request, view):
        if request.user.groups.all()[0].name == 'user':
            return True
        else:
            return False

