from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import User, Student


# class UserSerializer(serializers.Serializer):
#     username = serializers.CharField(max_length=100)
#     email = serializers.EmailField(max_length=90)
#     address = serializers.CharField(max_length=100)
#     password = serializers.CharField(max_length=20)


class UserSerializer(ModelSerializer):
    # def to_representation(instance):
    #     r =super().to_representation(instance)

    class Meta:
        model = User
        fields = '__all__'




class StudentSerializer(ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Student
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        s = UserSerializer(instance.user)
        response['user'] = s.data
        return response



# class StudentPostSerializer(ModelSerializer):
#
#     class Meta:
#         model = Student
#         fields = '__all__'
