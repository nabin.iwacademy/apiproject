#
# >>> user =['Nabin', 'Shrestha', '+977 9812345678']
# >>> name, lat_name, phone = user
# >>> name
# 'Nabin'
# >>> last_name
# 'Shrestha'
# >>> phone
# '+977 9812345678'
#
# >>> people = [user, ['Nepal', 'NPL', 'unlisted']]
# >>> for (name, last_name, phone) in people:
# ...      print (name, phone)
# ...
# Nabin +977 9812345678
# Nepal unlisted
#
# >>> Nabin, (g_name, g_last_name, g_phone) = people
# >>> g_name
# 'Nabin'
# >>> g_last_name
# 'NPL'
# >>> g_phone
# 'unlisted'
# >>> Nabin
# ['Nabin', 'Shrestha', '+977 9812345678']
#
#
#
# colors = ['red', 'blue', 'green', 'yellow']
#
#
# result = ''
# for s in colors:
#     result += s
#
#
#
# result = ''.join(colors)
#
#
#
#
# colors = ['red', 'blue', 'green', 'yellow']
# print ('Choose', ', '.join(colors[:-1]), \
#       'or', colors[-1])
# >> Choose red, blue, green or yellow
#
#
#
# # Do this :     # And not this :
# if x:             if x == True:
#    pass                  pass
# # Do this :     # And not this :
# if items:         if len(items) != 0:
#     pass                pass
# # and especially not that :
#         if items != []:
#                pass
#
#
#
# # With a loop :
# total = 0
# for num in range(1, 101):
#     total += num * num
#
#
# # With a list comprehension :
# total = sum([num * num for num in range(1, 101)])
# # With a generator expression :
# total = sum(num * num for num in range(1, 101))
#
#
# total = sum(num * num for num in range(1, 1000000000))
#
#
# new_list = []
# for item in a_list:
#     if condition(item):
#         new_list.append(fn(item))
#
#
# new_list = [fn(item) for item in a_list if condition(item)]
#
#
#
#
