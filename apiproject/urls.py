"""apiproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from api.views import UserView, StudentView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter
from django.contrib.auth import views
r = DefaultRouter()
r.register('api/user', UserView, basename='user_api')
r.register('api/student', StudentView, basename='student_api')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    # path('blog/',include('blogapp.urls')),
    # path('api/user/', UserView.as_view({'get':'list','post':'create'}), name='home'),
    path('login/', obtain_auth_token, name="login"),
    path('', include('blogapp.urls')),
    path('users/', include('django.contrib.auth.urls')),


]+r.urls

