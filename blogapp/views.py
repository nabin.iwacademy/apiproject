from django.utils import timezone
from  django.shortcuts import  render, get_list_or_404,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (TemplateView,
                                  ListView,
                                  DetailView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView)

from .models import Comment, Post
from blogapp.forms import CommentForm, PostForm


class AboutView(TemplateView):
    template_name = 'about.html'


class PostListView(ListView):
    model = Post

    def get_queryset(self):
        return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')


class PostDetailView(DetailView):
    model = Post


class CreatePostView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    redirect_field_name = 'blogapp/post_detail.html'
    form_class = PostForm
    model = Post


class PostUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    redirect_field_name = 'blogapp/post_detail.html'
    form_class = PostForm
    model = Post


class PostDeleteView(LoginRequiredMixin, DeleteView):
        model = Post
        success_url = reverse_lazy('post_list')

class DraftListView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    redirect_field_name = 'blogapp/post_list.html'
    model = Post

    def get_queryset(self):
        return Post.objects.filter(published_date__isnull =True).order_by('-published_date')

#####################################
#####################################
@login_required
def post_publish(request, pk):
    post = get_list_or_404(Post, pk=pk)
    post.publish
    return redirect('post_detail_view', pk=pk)
@login_required
def add_comment_to_post(request, pk):
    post = get_list_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        comment = form.save(commit=False)
        comment.save()
        return redirect('post_detail_view', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'blogapp/comment_form.html',{'form':form})

@login_required
def comment_approve(request, pk):
    comment = get_list_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('post_detail_view',pk=comment.post.pk)

@login_required
def comment_remove(request, pk):
    comment = get_list_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('post_detail_view', pk=post_pk)